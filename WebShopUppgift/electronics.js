document.addEventListener('DOMContentLoaded', function () {
  let products = document.querySelector('.products');
  let prod = document.querySelector('.prod');

  async function fetchProducts(url) {
    try {
      let data = await fetch(url);
      let response = await data.json();
      for (let i = 8; i <=13; i++) {
        let description = response[i].description;
        let title = response[i].title;
        products.innerHTML +=
          `<div class="product">
              <div class="product-content">
              <h2 class="product-title">${title.length > 40 ? title.substring(0, 40).concat(' ...') : title}</h2>
              <img class="product-img" src="${response[i].image}" alt="${response[i].category.name}">
              <h4 class="product-category">${response[i].category}</h4>
              <p class="product-description">${description.length > 80 ? description.substring(0, 80).concat(' ...') : description}</p>
              <div class="product-price-container">
                <h3 class="product-price">$ ${response[i].price}</h3>
                <a class="add-to-cart" href="#" data-productID="${response[i].id}">Add to Cart  <ion-icon name="bag-outline"></ion-icon></a>
              </div>
              </div>
              </div>`;
      }
    } catch (err) {
      console.log(err);
    }
  }
  fetchProducts('https://fakestoreapi.com/products');
});
