/*const api = {
  getAllProducts: () => {
    return fetch('https://fakestoreapi.com/products')
      .then(res => res.json())
    //  .then(json => console.log(json))
      .catch(err => console.log(err))
  }
}

api.getAllProducts()
  .then((data) => {
   // console.log(data[2].title)
   // document.getElementById('test')
   //   .innerHTML = data[2].title
    let cardsToHtml = "";
    data.map((products) => {
      console.log(products)
    })

  })
*/

document.addEventListener('DOMContentLoaded', function () {
  let products = document.querySelector('.products');
  let prod = document.querySelector('.prod');

  async function fetchProducts(url) {
    try {
      let data = await fetch(url);
      let response = await data.json();
      for (let i = 0; response.length; i++) {
        let description = response[i].description;
        let title = response[i].title;
        products.innerHTML +=
          `<div class="product">
              <div class="product-content">
              <h2 class="product-title">${title.length > 40 ? title.substring(0, 40).concat(' ...') : title}</h2>
              <img class="product-img" src="${response[i].image}" alt="${response[i].category.name}">
              <h4 class="product-category">${response[i].category}</h4>
              <p class="product-description">${description.length > 80 ? description.substring(0, 80).concat(' ...') : description}</p>
              <div class="product-price-container">
                <h3 class="product-price">$ ${response[i].price}</h3>
                <a class="add-to-cart" href="#" data-productID="${response[i].id}">Add to Cart  <ion-icon name="bag-outline"></ion-icon></a>
              </div>
              </div>
              </div>`;
      }
    } catch (err) {
      console.log(err);
    }
  }
  fetchProducts('https://fakestoreapi.com/products');
});
